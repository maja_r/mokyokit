public class DataFromFile{

    private int kontakt_id;
    private int klient_id;
    private int pracownik_id;
    private String status;
    private String kontakt_ts;

    public DataFromFile(){

    }

    public DataFromFile (int kontakt_id, int klient_id, int pracownik_id, String status, String kontakt_ts){
        this.kontakt_id = kontakt_id;
        this.klient_id = klient_id;
        this.pracownik_id = pracownik_id;
        this.status = status;
        this.kontakt_ts = kontakt_ts;
    }


    public int getKontakt_id(){
        return kontakt_id;
    }
    public int getKlient_id(){
        return klient_id;
    }
    public int getPracownik_id(){
        return pracownik_id;
    }
    public String getStatus(){
        return status;
    }
    public String getKontakt_ts(){
        return kontakt_ts;
    }

    public void setKontakt_id(int kontakt_id){
        this.kontakt_id = kontakt_id;
    }
    public void setKlient_id(int klient_id){
        this.klient_id = klient_id;
    }
    public void setPracownik_id(int pracownik_id){
        this.pracownik_id = pracownik_id;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public void setKontakt_ts(String kontakt_ts){
        this.kontakt_ts = kontakt_ts;
    }

}
