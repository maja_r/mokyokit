import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvParser {

    private List<DataFromFile> _dataFromFile;
    private String _fileName;

    public CsvParser(List<DataFromFile> dataFromFile, String fileName){
        _dataFromFile = dataFromFile;
        _fileName = fileName;
    }

    public void generateCsvFile() {
        try
        {
            FileWriter writer = new FileWriter(_fileName);
            writer.append("kontakt_id");
            writer.append('|');
            writer.append("klient_id");
            writer.append('|');
            writer.append("pracownik_id");
            writer.append('|');
            writer.append("status");
            writer.append('|');
            writer.append("kontakt_ts");
            writer.append("\n");
            for(DataFromFile dataFromFile:_dataFromFile) {
                writer.append(Integer.toString(dataFromFile.getKontakt_id()));
                writer.append('|');
                writer.append(Integer.toString(dataFromFile.getKlient_id()));
                writer.append('|');
                writer.append(Integer.toString(dataFromFile.getPracownik_id()));
                writer.append('|');
                writer.append(dataFromFile.getStatus());
                writer.append('|');
                writer.append(dataFromFile.getKontakt_ts());
                writer.append("\n");
            }

            writer.flush();
            writer.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
