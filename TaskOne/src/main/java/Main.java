import java.io.File;
import java.io.IOException;
import java.util.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class Main {
    public static void main(String[] args) {

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            File file = new File("C:\\Users\\computer\\Desktop\\myit_dwh_zadania\\statuses.json");
            List<DataFromFile> dataFromFile = objectMapper.readValue(file, new TypeReference<List<DataFromFile>>(){});

            Collections.sort(dataFromFile,new DataFromFileComparator());

            CsvParser csvParser = new CsvParser(dataFromFile, "C:\\Users\\computer\\Desktop\\myit_dwh_zadania\\statuses.csv");
            csvParser.generateCsvFile();

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}

