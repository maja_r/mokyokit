import java.util.Comparator;

class DataFromFileComparator implements Comparator<DataFromFile> {
    public int compare(DataFromFile dff1, DataFromFile dff2){
        int sort;
        sort = Integer.compare(dff1.getKlient_id(), dff2.getKlient_id());
        if(sort == 0){
            sort = dff1.getKontakt_ts().compareTo(dff2.getKontakt_ts());
        }
        return sort;
    }
}
